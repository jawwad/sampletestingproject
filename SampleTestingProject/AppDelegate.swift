//
//  AppDelegate.swift
//  SampleTestingProject
//
//  Created by Jawwad Ahmad on 4/26/18.
//  Copyright © 2018 Jawwad Ahmad. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

  var window: UIWindow?

  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
    return true
  }
}
