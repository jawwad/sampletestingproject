//
//  SampleTestingProjectTests.swift
//  SampleTestingProjectTests
//
//  Created by Jawwad Ahmad on 4/26/18.
//  Copyright © 2018 Jawwad Ahmad. All rights reserved.
//

import XCTest
@testable import SampleTestingProject

class FirstTestCase: XCTestCase {
    
  override func setUp() {
    super.setUp()
    logMethod()
  }

  override func tearDown() {
    super.tearDown()
    logMethod()
  }

  func testOne() throws {
    XCTAssertEqual(1 + 1, 2)
  }

  func testTwo() {
    XCTAssertEqual(2 + 2, 4)
  }

  func testPerformanceExample() {
    // This is an example of a performance test case.
    self.measure {
      var sum = 0
      let n = 100
      for i in 1...n {
        sum += i
      }
      usleep(1_000_000/10) // Sleep for one tenth of a second
      let expectedSum = n * (n + 1) / 2
      XCTAssertEqual(sum, expectedSum)
    }
  }    
}
