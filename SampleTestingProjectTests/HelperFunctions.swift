//
//  HelperFunctions.swift
//  SampleTestingProjectTests
//
//  Created by Jawwad Ahmad on 4/26/18.
//  Copyright © 2018 Jawwad Ahmad. All rights reserved.
//

import Foundation

func logMethod(function: String = #function, file: String = #file) {
  print("Calling", function, "in", (file as NSString).lastPathComponent)
}
