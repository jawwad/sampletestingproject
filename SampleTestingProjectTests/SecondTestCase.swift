//
//  SecondTestCase.swift
//  SampleTestingProjectTests
//
//  Created by Jawwad Ahmad on 4/26/18.
//  Copyright © 2018 Jawwad Ahmad. All rights reserved.
//

import XCTest

class SecondTestCase: XCTestCase {
    
  override func setUp() {
    super.setUp()
    logMethod()
  }

  override func tearDown() {
    super.tearDown()
    logMethod()
  }
    
  func testThree() throws {
    XCTAssertEqual(3 + 3, 6)
  }

  func testFour() {
    XCTAssertEqual(4 + 4, 8)
  }
}
